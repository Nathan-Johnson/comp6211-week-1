﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //part 1
            Console.WriteLine("Hello");
            Console.WriteLine("Nathan");
            Console.ReadKey();
            Console.Clear();

            //part 2
            var sum1 = 74;
            var sum2 = 36;
            Console.WriteLine($"{sum1} + {sum2} = {sum1+sum2}");
            Console.ReadKey();
            Console.Clear();

            //part 3
            var div1 = 50;
            var div2 = 3;
            Console.WriteLine($"{div1} / {div2} = {div1/div2}");
            Console.ReadKey();
            Console.Clear();

            //part 4
            var input1 = 0;
            var input2 = 0;
            Console.WriteLine("Please input first number");
            input1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Please input second number");
            input2 = int.Parse(Console.ReadLine());
            Console.WriteLine($"{input1} X {input2} = {input1*input2}");
            Console.ReadKey();
            Console.Clear();

            //part 5
            var mult1 = 0;
            var loopcount = 1;
            Console.WriteLine("Please input number");
            mult1 = int.Parse(Console.ReadLine());
            while (loopcount <11)
             {
                Console.WriteLine($"{mult1} X {loopcount} = {mult1*loopcount}");
                loopcount++;
             }
            Console.ReadKey();
            Console.Clear();

            //part 6
            var rad = 7.5;
            var pi = 3.14159265359;
            Console.WriteLine($"Perimeter is = {2*pi*rad}");
            Console.WriteLine($"Area is = {pi*rad*rad}");
            Console.ReadKey();
            Console.Clear();

            //part 7
            var usernum = 0;
            Console.WriteLine("Please input number");
            usernum = int.Parse(Console.ReadLine());
            if(usernum> 0)
                {
                    Console.WriteLine("Number is Positive");
                }
            else
                {
                   Console.WriteLine("Number is Negative");
                }
            Console.ReadKey();
            Console.Clear();

            //part 8
            var rank1 = 0;
            var rank2 = 0;
            var rank3 = 0;
            Console.WriteLine("Please input first number");
            rank1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Please input Second number");
            rank2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Please input Third number");
            rank3 = int.Parse(Console.ReadLine());

            if(rank1 > rank2)
            {
                if(rank1 > rank3)
                    {Console.WriteLine($"{rank1} is largest");}
            }
            else
            {
                if(rank2 > rank3)
                    {Console.WriteLine($"{rank2} is largest");}
                else
                    {Console.WriteLine($"{rank3} is largest");}
            }
            Console.ReadKey();
            Console.Clear();

        }
    }
}
